﻿using UnityEngine;
public class CameraController : MonoBehaviour
    {   
        public Transform knife; 
        public float offsetY; 
        public float offsetZ; 

        void LateUpdate()
        {
            transform.position = new Vector3(transform.position.x, knife.position.y + offsetY, transform.position.z);
            
            transform.position = new Vector3(transform.position.x, transform.position.y, knife.position.z + offsetZ);
        }
       
    
    }

