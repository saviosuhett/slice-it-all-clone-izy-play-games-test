using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiController : MonoBehaviour
{
    public GameObject TapToFlipObj;
    public GameObject gameOverModal;
    public GameObject levelWinModal;
    private void Subscribe()
    {
        GameEvents.OnGameStart += SetUI;
        GameEvents.OnPlayerDeath += GameOver;
        GameEvents.OnLevelFinish += LevelWin;

    }
    
    private void UnSubscribe()
    {
        GameEvents.OnGameStart -= SetUI;
        GameEvents.OnPlayerDeath -= GameOver;
        GameEvents.OnLevelFinish -= LevelWin;
    }

    private void SetUI()
    {
        TapToFlipObj.SetActive(false);
    }

    private void Awake()
    {
        Subscribe();
    }

    private void OnDestroy()
    {
        UnSubscribe();
    }

    void GameOver()
    {
        if(gameObject != null)
            gameOverModal.SetActive(true);
    }

    void LevelWin()
    {
        levelWinModal.SetActive(true);
    }
}
