using UnityEngine;
using UnityEngine.SceneManagement;

public static class ReloadScene
{

    public static void ReloadCurrentScene()
    {
        
        var currentScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentScene);
        
    }
}