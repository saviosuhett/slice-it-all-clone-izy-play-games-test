using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Cube : MonoBehaviour
{
    public GameObject fruitSlicedPrefab;
    public float startForce = 15f;
    [SerializeField] private int cointOnDestroyCube = 1;
    
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Knife")
        {
            Vector3 position = transform.position;
            
            Vector3 direction = (other.transform.position - position).normalized;

            Quaternion rotation = Quaternion.LookRotation(direction);

            GameObject slicedFruit = Instantiate(fruitSlicedPrefab, transform.position, rotation);

            //Destroy(slicedFruit, 3f);
            Destroy(gameObject);
            GameEvents.GainPoints?.Invoke(cointOnDestroyCube);
        }
    }
}
