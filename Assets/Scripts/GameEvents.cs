using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static Action <int> GainPoints;
    public static Action OnGameStart;
    public static Action OnPlayerDeath;
    public static Action OnLevelFinish;

    private void Awake()
    {
        GainPoints += OnCubeDestroy;
    }

    public void OnCubeDestroy(int value)
    {
        Debug.Log("chamou aq" + value);
        Coin += value;
        
    }
    
    public static int Coin
    {
        get => PlayerPrefs.GetInt("Coins");
        private set => PlayerPrefs.SetInt("Coins", value);
    }

    private void OnDestroy()
    {
        GainPoints -= OnCubeDestroy;
    }
}
