using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class BaseModal : MonoBehaviour
{
    public Button okBtn;
    void Start()
    {
        okBtn.onClick.AddListener(()=> ReloadScene.ReloadCurrentScene());
    }
    
}
