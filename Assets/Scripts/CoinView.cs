
using TMPro;
using UnityEngine;

public class CoinView : MonoBehaviour
{
    [SerializeField] private TMP_Text coinText;

    void Update()
    {
        coinText.text = GameEvents.Coin.ToString();
    }
}
