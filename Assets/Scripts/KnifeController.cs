using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeController : MonoBehaviour
{

    public float forwardForce; 
    public float jumpForce; 

    private bool isMoving = false;
    private Rigidbody rb;
    private Animator animator;
    
    private bool isInitGame = false;
    public static bool isKnifeClickable = true;
    
  

    void Start()
    {
        isKnifeClickable = true;
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isKnifeClickable)
        {
            JumpForward();
        }
    }

    void JumpForward()
    {
        if(isInitGame is false)
            GameEvents.OnGameStart?.Invoke();

        animator.SetTrigger("Rotate");

        Vector3 forwardForceVector = transform.forward * forwardForce;
        rb.AddForce(forwardForceVector);
        
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
    
    private void OnCollisionEnter(Collision other)
    {
        rb.constraints = RigidbodyConstraints.FreezePositionX  | RigidbodyConstraints.FreezeRotationX;

        if (other.gameObject.CompareTag("Floor"))
            Die();
    }

    private void Die()
    {
        isKnifeClickable = false;
        GameEvents.OnPlayerDeath?.Invoke();
    }

    void PlayAnim()
    {
        animator.SetBool("Rotate", true);
    }
}
