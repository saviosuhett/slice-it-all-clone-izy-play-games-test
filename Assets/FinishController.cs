using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishController : MonoBehaviour
{
    public ParticleSystem particleSystem;


    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Knife"))
        {
            KnifeController.isKnifeClickable = false;
            GameEvents.OnLevelFinish?.Invoke();
            PlayParticle();
        }
           
    }
    
    void PlayParticle()
    {
        particleSystem.Play();
    }
}
